extends Node


@onready var score_label = $ScoreLabel

var score = 0


func add_point(point: int):
	score += point
	score_label.text = str(score)
