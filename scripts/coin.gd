extends Area2D

@onready var animation_player = $AnimationPlayer
@onready var game_manager = %GameManager


func _on_body_entered(body):
	game_manager.add_point(1)
	animation_player.play("pickup")
